function plotData() {
  var graph = document.getElementById("graph-new");
  graph.innerHTML = "";
  var mine = document.createElement("div");
  mine.id = "mineeee";
  mine.classList.add("graph-style");
  graph.append(mine);
  mine = document.createElement("div");
  mine.id = "squarewave";
  mine.classList.add("graph-style");
  graph.append(mine);
  mine = document.createElement("div");
  mine.id = "squarewaveResistor";
  mine.classList.add("graph-style");
  graph.append(mine);
  mine = document.createElement("div");
  mine.id = "squarewaveInductor";
  mine.classList.add("graph-style");
  graph.append(mine);
  

  a = generateData();
  console.log(a);
  var data = [
    {
      x: a[0][1],
      y: a[0][0],
      mode: "lines",
      name: "VINP",
    },
    // {
    //   x: a[1][1],
    //   y: a[1][0],
    //   mode: "lines",
    //   // name: values["VM7"]["name"],
    // },
    {
      x: [0],
      y: [0],
      mode: "lines",
      name: "    ",
      marker: {
        color: "White",
      },
    },
    
  ];
  console.log([-1 * (a[3][1] + 1), a[3][1] + 1]);
  var layout = {
    xaxis: { range: [0, 0.041], title: "Time(s)" },
    yaxis: { range: [-1 * (a[5][1] + 1), a[5][1] + 1], title: "Voltage(V)" },
    margin: { t: 20 },
  };

  Plotly.newPlot("mineeee", data, layout);
  Plotly.newPlot(
    "squarewave",
    [
      {
        x: a[2][1],
        y: a[2][0],
        mode: "lines",
        name: values["VM3"]["name"],
      },
      {
        x: [0],
        y: [0],
        mode: "lines",
        name: "   ",
        marker: {
          color: "White",
        },
      },
    ],
    {
      xaxis: { range: [0, 0.041], title: "Time(s)" },
      yaxis: { range: [-1 * (a[5][0] + 1), a[5][0] + 1], title: "Voltage(V)" },
      margin: { t: 20 },
    }
  );
  Plotly.newPlot(
    "squarewaveInductor",
    [
      {
        x: a[3][1],
        y: a[3][0],
        mode: "lines",
        name: values["VM4"]["name"],
        marker:{
          color:"Green"
        }
      },
      {
        x: [0],
        y: [0],
        mode: "lines",
        name: "   ",
        marker: {
          color: "White",
        },
      },
    ],
    {
      xaxis: { range: [0, 0.041], title: "Time(s)" },
      yaxis: { range: [-1 * (a[5][0] + 1), a[5][0] + 1], title: "Voltage(V)" },
      margin: { t: 20 },
    }
  );
  Plotly.newPlot(
    "squarewaveResistor",
    [
      {
        x: a[4][1],
        y: a[4][0],
        mode: "lines",
        name: values["VM1"]["name"],
        marker:{
          color:"Green"
        }
      },
      {
        x: [0],
        y: [0],
        mode: "lines",
        name: "   ",
        marker: {
          color: "White",
        },
      },
    ],
    {
      xaxis: { range: [0, 0.041], title: "Time(s)" },
      yaxis: { range: [-1 * (a[5][0] + 1), a[5][0] + 1], title: "Voltage(V)" },
      margin: { t: 20 },
    }
  );
}

function generateData() {
  var a = values["AC1"]["freq"];
  var dcvalue = values["DC1"]["value"];
  var aplitute = values["AC1"]["volt"];
  var yval = [];
  var xval = [];
  var value, valuetri;
  var large;
  var yvaltri = [];
  var app = values["AC2"]["volt"];
  var ac2freq = values["AC2"]["freq"];
  var distance = 1 / ac2freq;
  var flag = true;
  var parity = true;
  if (app <= aplitute) {
    large = app;
  } else {
    large = aplitute;
  }
  var difference = (1 / 100) * large;
  var slop1 = 4 * (app / distance);
  var slop2 = -1 * slop1;
  var sqwave = dcvalue,
    dist = 0;
  var sq = [];
  var cap1 = values["C1"]["value"];
  var cap2 = values["C2"]["value"];
  var sum = parseInt(cap1) + parseInt(cap2);
  var vloadind = [];
  var vloadresis = [];
  var vresis = -1 * (dcvalue * (cap1 / sum));
  var vinduc = dcvalue * (cap2 / sum);
  for (let x = 0; x <= 0.04; x += 0.000001) {
    value = aplitute * Math.sin(2 * Math.PI * a * x);
    yval.push(value);
    xval.push(x);
    if (flag) {
      valuetri = slop1 * x - slop1 * dist - app;
      if (valuetri > app) {
        dist = dist + distance;
        flag = false;
        valuetri = slop2 * x - slop2 * dist - app;
      }
    } else {
      valuetri = slop2 * x - slop2 * dist - app;
      if (valuetri < -1 * app) {
        flag = true;
        valuetri = slop1 * x - slop1 * dist - app;
      }
    }
    yvaltri.push(valuetri);
    if (parity) {
      if (Math.abs(value - valuetri) < difference) {
        if (sqwave == dcvalue) {
          sqwave = -1 * dcvalue;
          vresis = dcvalue * (cap2 / sum);
          vinduc = -1 * (dcvalue * (cap1 / sum));
        } else {
          sqwave = dcvalue;
          vresis = -1 * (dcvalue * (cap1 / sum));
          vinduc = dcvalue * (cap2 / sum);
        }
        parity = false;
      }
    }
    if (Math.abs(value - valuetri) > difference) {
      parity = true;
    }
    sq.push(sqwave);
    vloadind.push(vinduc);
    vloadresis.push(parseFloat(vresis));
  }
  var z;
  if (parseInt(app) >= parseInt( aplitute)) {
    z = app;
  } else {
    z = aplitute;
  }

  return [
    [yval, xval],
    [yvaltri, xval],
    [sq, xval],
    [vloadind, xval],
    [vloadresis, xval],
    [parseFloat(dcvalue), parseFloat(z)],
  ];
}
function Reset() {

  window.location.reload();
}

function showtable() {

  var r = document.getElementById("readings");
  if (r.style.display = "none") {
      r.style.display = "block";

  } else {
      r.style.display.toggle();
  }

}

function Print() {

  window.print();
}